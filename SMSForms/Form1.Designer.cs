﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oAplikaciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nastaveníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odeslatBezMezerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabpagePoslatSMS = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelPocetZnakuBezMezer = new System.Windows.Forms.Label();
            this.labelPocetZnaku = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonOdebratKontakt = new System.Windows.Forms.Button();
            this.buttonAddContact = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPridatCislo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPridatJmeno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPrijemce = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonOdeslatSMS = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxZprava = new System.Windows.Forms.TextBox();
            this.tabpageOdeslaneSMS = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.buttonOdeslaneSMSUpdate = new System.Windows.Forms.Button();
            this.tabulkaOdeslaneSMS = new System.Windows.Forms.ListView();
            this.prijemce = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.text = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.odeslano = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabpageInformace = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.aboutKreditlabel = new System.Windows.Forms.Label();
            this.aboutKreditHeaderLabel = new System.Windows.Forms.Label();
            this.labelInformaceUzivatel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabpageOAplikaci = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buildLabel = new System.Windows.Forms.Label();
            this.buttonZavritOAplikaci = new System.Windows.Forms.Button();
            this.labelverze = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelKredit = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nastaveníToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vždyNahořeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontrolaAktualizaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabpagePoslatSMS.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabpageOdeslaneSMS.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabpageInformace.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabpageOAplikaci.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.nastaveníToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(502, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oAplikaciToolStripMenuItem,
            this.kontrolaAktualizaceToolStripMenuItem,
            this.toolStripSeparator1,
            this.konecToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // oAplikaciToolStripMenuItem
            // 
            this.oAplikaciToolStripMenuItem.Name = "oAplikaciToolStripMenuItem";
            this.oAplikaciToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.oAplikaciToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.oAplikaciToolStripMenuItem.Text = "O aplikaci";
            this.oAplikaciToolStripMenuItem.Click += new System.EventHandler(this.oAplikaciToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(219, 6);
            // 
            // konecToolStripMenuItem
            // 
            this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            this.konecToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.konecToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.konecToolStripMenuItem.Text = "Konec";
            this.konecToolStripMenuItem.Click += new System.EventHandler(this.konecToolStripMenuItem_Click);
            // 
            // nastaveníToolStripMenuItem
            // 
            this.nastaveníToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.odeslatBezMezerToolStripMenuItem,
            this.toolStripSeparator2,
            this.nastaveníToolStripMenuItem1});
            this.nastaveníToolStripMenuItem.Name = "nastaveníToolStripMenuItem";
            this.nastaveníToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.nastaveníToolStripMenuItem.Text = "Nastavení";
            // 
            // odeslatBezMezerToolStripMenuItem
            // 
            this.odeslatBezMezerToolStripMenuItem.Name = "odeslatBezMezerToolStripMenuItem";
            this.odeslatBezMezerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.odeslatBezMezerToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.odeslatBezMezerToolStripMenuItem.Text = "Odeslat bez mezer";
            this.odeslatBezMezerToolStripMenuItem.Click += new System.EventHandler(this.odeslatBezMezerToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabpagePoslatSMS);
            this.tabControl1.Controls.Add(this.tabpageOdeslaneSMS);
            this.tabControl1.Controls.Add(this.tabpageInformace);
            this.tabControl1.Controls.Add(this.tabpageOAplikaci);
            this.tabControl1.Location = new System.Drawing.Point(12, 77);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(478, 312);
            this.tabControl1.TabIndex = 2;
            // 
            // tabpagePoslatSMS
            // 
            this.tabpagePoslatSMS.Controls.Add(this.groupBox1);
            this.tabpagePoslatSMS.Location = new System.Drawing.Point(4, 22);
            this.tabpagePoslatSMS.Name = "tabpagePoslatSMS";
            this.tabpagePoslatSMS.Padding = new System.Windows.Forms.Padding(3);
            this.tabpagePoslatSMS.Size = new System.Drawing.Size(470, 286);
            this.tabpagePoslatSMS.TabIndex = 0;
            this.tabpagePoslatSMS.Text = "Odeslat SMS";
            this.tabpagePoslatSMS.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelPocetZnakuBezMezer);
            this.groupBox1.Controls.Add(this.labelPocetZnaku);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxPrijemce);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.buttonOdeslatSMS);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxZprava);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(458, 274);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Formulář pro odeslání SMS";
            // 
            // labelPocetZnakuBezMezer
            // 
            this.labelPocetZnakuBezMezer.AutoSize = true;
            this.labelPocetZnakuBezMezer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPocetZnakuBezMezer.Location = new System.Drawing.Point(117, 255);
            this.labelPocetZnakuBezMezer.Name = "labelPocetZnakuBezMezer";
            this.labelPocetZnakuBezMezer.Size = new System.Drawing.Size(13, 13);
            this.labelPocetZnakuBezMezer.TabIndex = 8;
            this.labelPocetZnakuBezMezer.Text = "?";
            // 
            // labelPocetZnaku
            // 
            this.labelPocetZnaku.AutoSize = true;
            this.labelPocetZnaku.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPocetZnaku.Location = new System.Drawing.Point(108, 236);
            this.labelPocetZnaku.Name = "labelPocetZnaku";
            this.labelPocetZnaku.Size = new System.Drawing.Size(13, 13);
            this.labelPocetZnaku.TabIndex = 7;
            this.labelPocetZnaku.Text = "?";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonOdebratKontakt);
            this.groupBox4.Controls.Add(this.buttonAddContact);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.textBoxPridatCislo);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.textBoxPridatJmeno);
            this.groupBox4.Location = new System.Drawing.Point(223, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(229, 109);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Přidat kontakt";
            // 
            // buttonOdebratKontakt
            // 
            this.buttonOdebratKontakt.Enabled = false;
            this.buttonOdebratKontakt.Location = new System.Drawing.Point(22, 77);
            this.buttonOdebratKontakt.Name = "buttonOdebratKontakt";
            this.buttonOdebratKontakt.Size = new System.Drawing.Size(75, 23);
            this.buttonOdebratKontakt.TabIndex = 9;
            this.buttonOdebratKontakt.Text = "Odebrat";
            this.buttonOdebratKontakt.UseVisualStyleBackColor = true;
            this.buttonOdebratKontakt.Click += new System.EventHandler(this.buttonOdebratKontakt_Click);
            // 
            // buttonAddContact
            // 
            this.buttonAddContact.Location = new System.Drawing.Point(131, 77);
            this.buttonAddContact.Name = "buttonAddContact";
            this.buttonAddContact.Size = new System.Drawing.Size(75, 23);
            this.buttonAddContact.TabIndex = 8;
            this.buttonAddContact.Text = "Přidat";
            this.buttonAddContact.UseVisualStyleBackColor = true;
            this.buttonAddContact.Click += new System.EventHandler(this.buttonAddContact_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Číslo:";
            // 
            // textBoxPridatCislo
            // 
            this.textBoxPridatCislo.Location = new System.Drawing.Point(69, 49);
            this.textBoxPridatCislo.MaxLength = 14;
            this.textBoxPridatCislo.Name = "textBoxPridatCislo";
            this.textBoxPridatCislo.Size = new System.Drawing.Size(137, 20);
            this.textBoxPridatCislo.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Jméno:";
            // 
            // textBoxPridatJmeno
            // 
            this.textBoxPridatJmeno.Location = new System.Drawing.Point(69, 22);
            this.textBoxPridatJmeno.MaxLength = 20;
            this.textBoxPridatJmeno.Name = "textBoxPridatJmeno";
            this.textBoxPridatJmeno.Size = new System.Drawing.Size(137, 20);
            this.textBoxPridatJmeno.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Příjemce";
            // 
            // textBoxPrijemce
            // 
            this.textBoxPrijemce.Location = new System.Drawing.Point(16, 50);
            this.textBoxPrijemce.MaxLength = 14;
            this.textBoxPrijemce.Name = "textBoxPrijemce";
            this.textBoxPrijemce.Size = new System.Drawing.Size(181, 20);
            this.textBoxPrijemce.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listView1);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(223, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(229, 141);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kontakty";
            // 
            // listView1
            // 
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.Location = new System.Drawing.Point(6, 19);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(217, 116);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Jméno";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Telefon";
            this.columnHeader2.Width = 100;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(0, 147);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 66);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kontakty";
            // 
            // buttonOdeslatSMS
            // 
            this.buttonOdeslatSMS.Enabled = false;
            this.buttonOdeslatSMS.Location = new System.Drawing.Point(16, 236);
            this.buttonOdeslatSMS.Name = "buttonOdeslatSMS";
            this.buttonOdeslatSMS.Size = new System.Drawing.Size(78, 23);
            this.buttonOdeslatSMS.TabIndex = 4;
            this.buttonOdeslatSMS.Text = "Odeslat SMS";
            this.buttonOdeslatSMS.UseVisualStyleBackColor = true;
            this.buttonOdeslatSMS.Click += new System.EventHandler(this.buttonOdeslatSMS_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Zpráva";
            // 
            // textBoxZprava
            // 
            this.textBoxZprava.Location = new System.Drawing.Point(16, 98);
            this.textBoxZprava.MaxLength = 306;
            this.textBoxZprava.Multiline = true;
            this.textBoxZprava.Name = "textBoxZprava";
            this.textBoxZprava.Size = new System.Drawing.Size(181, 130);
            this.textBoxZprava.TabIndex = 3;
            this.textBoxZprava.TextChanged += new System.EventHandler(this.textBoxZprava_TextChanged);
            // 
            // tabpageOdeslaneSMS
            // 
            this.tabpageOdeslaneSMS.Controls.Add(this.groupBox6);
            this.tabpageOdeslaneSMS.Location = new System.Drawing.Point(4, 22);
            this.tabpageOdeslaneSMS.Name = "tabpageOdeslaneSMS";
            this.tabpageOdeslaneSMS.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageOdeslaneSMS.Size = new System.Drawing.Size(470, 286);
            this.tabpageOdeslaneSMS.TabIndex = 1;
            this.tabpageOdeslaneSMS.Text = "Odeslané SMS";
            this.tabpageOdeslaneSMS.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.buttonOdeslaneSMSUpdate);
            this.groupBox6.Controls.Add(this.tabulkaOdeslaneSMS);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(458, 274);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Seznam odeslaných SMS";
            // 
            // buttonOdeslaneSMSUpdate
            // 
            this.buttonOdeslaneSMSUpdate.Location = new System.Drawing.Point(368, 236);
            this.buttonOdeslaneSMSUpdate.Name = "buttonOdeslaneSMSUpdate";
            this.buttonOdeslaneSMSUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonOdeslaneSMSUpdate.TabIndex = 1;
            this.buttonOdeslaneSMSUpdate.Text = "Aktualizace";
            this.buttonOdeslaneSMSUpdate.UseVisualStyleBackColor = true;
            this.buttonOdeslaneSMSUpdate.Click += new System.EventHandler(this.buttonOdeslaneSMSUpdate_Click);
            // 
            // tabulkaOdeslaneSMS
            // 
            this.tabulkaOdeslaneSMS.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.prijemce,
            this.text,
            this.odeslano});
            this.tabulkaOdeslaneSMS.FullRowSelect = true;
            this.tabulkaOdeslaneSMS.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tabulkaOdeslaneSMS.Location = new System.Drawing.Point(17, 29);
            this.tabulkaOdeslaneSMS.MultiSelect = false;
            this.tabulkaOdeslaneSMS.Name = "tabulkaOdeslaneSMS";
            this.tabulkaOdeslaneSMS.Size = new System.Drawing.Size(345, 230);
            this.tabulkaOdeslaneSMS.TabIndex = 0;
            this.tabulkaOdeslaneSMS.UseCompatibleStateImageBehavior = false;
            this.tabulkaOdeslaneSMS.View = System.Windows.Forms.View.Details;
            this.tabulkaOdeslaneSMS.SelectedIndexChanged += new System.EventHandler(this.tabulkaOdeslaneSMS_SelectedIndexChanged);
            // 
            // prijemce
            // 
            this.prijemce.Text = "Příjemce";
            this.prijemce.Width = 100;
            // 
            // text
            // 
            this.text.Text = "Text";
            this.text.Width = 164;
            // 
            // odeslano
            // 
            this.odeslano.Text = "Odesláno";
            this.odeslano.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabpageInformace
            // 
            this.tabpageInformace.Controls.Add(this.groupBox5);
            this.tabpageInformace.Location = new System.Drawing.Point(4, 22);
            this.tabpageInformace.Name = "tabpageInformace";
            this.tabpageInformace.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageInformace.Size = new System.Drawing.Size(470, 286);
            this.tabpageInformace.TabIndex = 2;
            this.tabpageInformace.Text = "Informace o uživateli";
            this.tabpageInformace.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.aboutKreditlabel);
            this.groupBox5.Controls.Add(this.aboutKreditHeaderLabel);
            this.groupBox5.Controls.Add(this.labelInformaceUzivatel);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(458, 274);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Přihlášený uživatel";
            // 
            // aboutKreditlabel
            // 
            this.aboutKreditlabel.AutoSize = true;
            this.aboutKreditlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.aboutKreditlabel.Location = new System.Drawing.Point(40, 121);
            this.aboutKreditlabel.Name = "aboutKreditlabel";
            this.aboutKreditlabel.Size = new System.Drawing.Size(60, 24);
            this.aboutKreditlabel.TabIndex = 3;
            this.aboutKreditlabel.Text = "label7";
            this.aboutKreditlabel.Visible = false;
            // 
            // aboutKreditHeaderLabel
            // 
            this.aboutKreditHeaderLabel.AutoSize = true;
            this.aboutKreditHeaderLabel.Location = new System.Drawing.Point(16, 100);
            this.aboutKreditHeaderLabel.Name = "aboutKreditHeaderLabel";
            this.aboutKreditHeaderLabel.Size = new System.Drawing.Size(82, 13);
            this.aboutKreditHeaderLabel.TabIndex = 2;
            this.aboutKreditHeaderLabel.Text = "Zbývající kredit";
            this.aboutKreditHeaderLabel.Visible = false;
            // 
            // labelInformaceUzivatel
            // 
            this.labelInformaceUzivatel.AutoSize = true;
            this.labelInformaceUzivatel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInformaceUzivatel.Location = new System.Drawing.Point(40, 58);
            this.labelInformaceUzivatel.Name = "labelInformaceUzivatel";
            this.labelInformaceUzivatel.Size = new System.Drawing.Size(60, 24);
            this.labelInformaceUzivatel.TabIndex = 1;
            this.labelInformaceUzivatel.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Aktuálně přihlášený uživatel:";
            // 
            // tabpageOAplikaci
            // 
            this.tabpageOAplikaci.Controls.Add(this.groupBox7);
            this.tabpageOAplikaci.Location = new System.Drawing.Point(4, 22);
            this.tabpageOAplikaci.Name = "tabpageOAplikaci";
            this.tabpageOAplikaci.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageOAplikaci.Size = new System.Drawing.Size(470, 286);
            this.tabpageOAplikaci.TabIndex = 3;
            this.tabpageOAplikaci.Text = "O aplikaci";
            this.tabpageOAplikaci.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buildLabel);
            this.groupBox7.Controls.Add(this.buttonZavritOAplikaci);
            this.groupBox7.Controls.Add(this.labelverze);
            this.groupBox7.Controls.Add(this.linkLabel1);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Controls.Add(this.pictureBox3);
            this.groupBox7.Controls.Add(this.richTextBox1);
            this.groupBox7.Controls.Add(this.pictureBox2);
            this.groupBox7.Location = new System.Drawing.Point(3, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(461, 274);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "O aplikaci";
            // 
            // buildLabel
            // 
            this.buildLabel.AutoSize = true;
            this.buildLabel.Location = new System.Drawing.Point(340, 249);
            this.buildLabel.Name = "buildLabel";
            this.buildLabel.Size = new System.Drawing.Size(13, 13);
            this.buildLabel.TabIndex = 7;
            this.buildLabel.Text = "?";
            // 
            // buttonZavritOAplikaci
            // 
            this.buttonZavritOAplikaci.Location = new System.Drawing.Point(143, 244);
            this.buttonZavritOAplikaci.Name = "buttonZavritOAplikaci";
            this.buttonZavritOAplikaci.Size = new System.Drawing.Size(75, 23);
            this.buttonZavritOAplikaci.TabIndex = 6;
            this.buttonZavritOAplikaci.Text = "Zavřít";
            this.buttonZavritOAplikaci.UseVisualStyleBackColor = true;
            this.buttonZavritOAplikaci.Click += new System.EventHandler(this.buttonZavritOAplikaci_Click);
            // 
            // labelverze
            // 
            this.labelverze.AutoSize = true;
            this.labelverze.Location = new System.Drawing.Point(340, 228);
            this.labelverze.Name = "labelverze";
            this.labelverze.Size = new System.Drawing.Size(13, 13);
            this.labelverze.TabIndex = 5;
            this.labelverze.Text = "?";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(140, 178);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(129, 13);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "vymetalik.libor@vymak.cz";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(139, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Libor Vymětalík";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::WindowsFormsApplication1.Properties.Resources._12049;
            this.pictureBox3.Location = new System.Drawing.Point(18, 144);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(105, 105);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(18, 88);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(423, 40);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "Aplikace pro odesílání SMS přes garantovanou bránu. Odesílání se realizuje pomocí" +
    " služby sms.sluzba.cz. Cena za odeslání sms do ČR je od 0,70 kč a do zahraničí o" +
    "d 1,80 kč bez DPH";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WindowsFormsApplication1.Properties.Resources.logo_male;
            this.pictureBox2.Location = new System.Drawing.Point(18, 31);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(345, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // labelKredit
            // 
            this.labelKredit.AutoSize = true;
            this.labelKredit.Location = new System.Drawing.Point(396, 46);
            this.labelKredit.Name = "labelKredit";
            this.labelKredit.Size = new System.Drawing.Size(35, 13);
            this.labelKredit.TabIndex = 3;
            this.labelKredit.Text = "label3";
            this.labelKredit.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsFormsApplication1.Properties.Resources.logo_male;
            this.pictureBox1.Location = new System.Drawing.Point(12, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(338, 45);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(209, 6);
            // 
            // nastaveníToolStripMenuItem1
            // 
            this.nastaveníToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vždyNahořeToolStripMenuItem});
            this.nastaveníToolStripMenuItem1.Name = "nastaveníToolStripMenuItem1";
            this.nastaveníToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.nastaveníToolStripMenuItem1.Text = "Nastavení";
            // 
            // vždyNahořeToolStripMenuItem
            // 
            this.vždyNahořeToolStripMenuItem.Name = "vždyNahořeToolStripMenuItem";
            this.vždyNahořeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.vždyNahořeToolStripMenuItem.Text = "Vždy nahoře";
            this.vždyNahořeToolStripMenuItem.Click += new System.EventHandler(this.vždyNahořeToolStripMenuItem_Click);
            // 
            // kontrolaAktualizaceToolStripMenuItem
            // 
            this.kontrolaAktualizaceToolStripMenuItem.Name = "kontrolaAktualizaceToolStripMenuItem";
            this.kontrolaAktualizaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.kontrolaAktualizaceToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.kontrolaAktualizaceToolStripMenuItem.Text = "Kontrola aktualizace";
            this.kontrolaAktualizaceToolStripMenuItem.Click += new System.EventHandler(this.kontrolaAktualizaceToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 401);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelKredit);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Odesílání SMS přes garantovanou bránu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabpagePoslatSMS.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabpageOdeslaneSMS.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tabpageInformace.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabpageOAplikaci.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabpagePoslatSMS;
        private System.Windows.Forms.TabPage tabpageOdeslaneSMS;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOdeslatSMS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxZprava;
        private System.Windows.Forms.Label labelKredit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPrijemce;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonAddContact;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPridatCislo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPridatJmeno;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button buttonOdebratKontakt;
        private System.Windows.Forms.TabPage tabpageInformace;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelInformaceUzivatel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelPocetZnaku;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button buttonOdeslaneSMSUpdate;
        private System.Windows.Forms.ListView tabulkaOdeslaneSMS;
        private System.Windows.Forms.ColumnHeader prijemce;
        private System.Windows.Forms.ColumnHeader text;
        private System.Windows.Forms.ColumnHeader odeslano;
        private System.Windows.Forms.Label labelPocetZnakuBezMezer;
        private System.Windows.Forms.ToolStripMenuItem nastaveníToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odeslatBezMezerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oAplikaciToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage tabpageOAplikaci;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelverze;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonZavritOAplikaci;
        private System.Windows.Forms.Label buildLabel;
        private System.Windows.Forms.Label aboutKreditlabel;
        private System.Windows.Forms.Label aboutKreditHeaderLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem nastaveníToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vždyNahořeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontrolaAktualizaceToolStripMenuItem;

    }
}

