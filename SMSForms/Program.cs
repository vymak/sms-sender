﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Random r = new Random();
            int a = r.Next(1, 13);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            /*if (a % 2 == 0)
                Application.Run(new Form1());
            else*/
                Application.Run(new Form1());
        }
    }
}
