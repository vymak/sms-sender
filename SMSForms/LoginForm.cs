﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Classes;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class LoginForm : Form
    {
        NastavLogin nastav;
        Form1 form;

        public LoginForm()
        {
            InitializeComponent();
            nastav = new NastavLogin();
        }

        private void saveUserAccountCheckbox_CheckedChanged(object sender, EventArgs e)
        {

            if (saveUserAccountCheckbox.Checked)
            {
                DialogResult res = MessageBox.Show("Přihlašovací údaje budou uložena na Váš disk. Data jsou zakódována v binární formě. Přesto doporučuji být nanejvýš obezřetný.\n\nPřejete si přesto uložit data na disk?", "Upozornění", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);


                if (res == DialogResult.No)
                    saveUserAccountCheckbox.Checked = false;
            }
        }

        private void stornoButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            nastav.nastavUzivatelskeUdaje(usernameTextBox.Text, passwordTextBox.Text);

            if (saveUserAccountCheckbox.Checked)
            {
                Serializace.serializuj(nastav);
            }

            form.nastavUcet(nastav);
            form.Enabled = true;
            this.Close();
        }

        public void nastavInstanci(Form1 instance)
        {
            form = instance;
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (nastav.getPassword == null && nastav.getUsername == null)
                Application.Exit();
        }

        private void usernameTextBox_TextChanged(object sender, EventArgs e)
        {
            kontrolaUdaju();
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            kontrolaUdaju();
        }

        private void kontrolaUdaju()
        {
            if (usernameTextBox.Text.Length != 0 && passwordTextBox.Text.Length != 0)
            {
                loginButton.Enabled = true;
            }
            else
            {
                loginButton.Enabled = false;
            }
        }

        private void linkLabelRegistrace_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://sms.sluzba.cz/users/new");
            this.TopMost = false;
        }
    }
}
