﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class Form1
    {
        private void buttonZavritOAplikaci_Click(object sender, EventArgs e)
        {
            if (tabControl1.TabPages.Contains(tabpageOAplikaci))
            {
                tabControl1.TabPages.Remove(tabpageOAplikaci);
            }
        }

        private void konecToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void oAplikaciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!tabControl1.TabPages.Contains(tabpageOAplikaci))
            {
                tabControl1.TabPages.Add(tabpageOAplikaci);
                tabControl1.SelectedTab = tabpageOAplikaci;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("http://www.vymak.cz");
            }
            catch (Exception)
            {
                MessageBox.Show("Chyba při spouštění klienta", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void vždyNahořeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!vždyNahořeToolStripMenuItem.Checked)
            {
                vždyNahořeToolStripMenuItem.Checked = true;
                this.TopMost = true;
            }
            else
            {
                vždyNahořeToolStripMenuItem.Checked = false;
                this.TopMost = false;
            }
        }

        private void kontrolaAktualizaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aktualizaceVerze();
        }
    }
}
