﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using sms_sluzba_cz.sms_gate;
using WindowsFormsApplication1.Classes;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        List<OutgoingSms> listSMS;
        LoginInfo log;
        NastavLogin nastav;
        private string credit = null;
        private string key = null;
        OutgoingSms sms;
        Contact kontakt;
        private bool odeslatBezMezer;

        public Form1()
        {
            InitializeComponent();
            nastav = new NastavLogin();
            log = new LoginInfo();
            sms = new OutgoingSms();
            mazaniZaloznichSouboru();
            kontrolaUctu();

            if (File.Exists("contact.bin"))
            {
                kontakt = SerializaceContact.deserializace();
                aktualizaceTabulky();
            }
            else
            {
                kontakt = new Contact();
            }


            labelPocetZnaku.Text = "";
            labelPocetZnakuBezMezer.Text = "";
            labelverze.Text = "Verze: " + Application.ProductVersion.ToString();
            buildLabel.Text = BuildClass.BuildDate.ToString();
            tabControl1.TabPages.Remove(tabpageOAplikaci);
            aktualizaceKredituPriSpusteni();
        }

        private void mazaniZaloznichSouboru()
        {
            #region MazaníZálohyVyjímka
            try
            {
                if (File.Exists("SMSPosilacBackup.exe"))
                    File.Delete("SMSPosilacBackup.exe");
            }
            catch (Exception e)
            {
                MessageBox.Show("Chyba při mazání zálohy:\n" + e.Message, "Chyba mazání", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }

        private void priradUcet()
        {
            log.Login = nastav.getUsername;
            log.Password = nastav.getPassword;
            labelInformaceUzivatel.Text = nastav.getUsername;
        }

        private void aktualizaceKredituPriSpusteni()
        {
            if (File.Exists("credit.bin"))
            {
                StreamReader read = new StreamReader("credit.bin");
                string temp = read.ReadLine();
                labelKredit.Text = "Kredit: " + temp + ",- Kč";
                labelKredit.Visible = true;
                aboutKreditHeaderLabel.Visible = true;
                aboutKreditlabel.Visible = true;
                aboutKreditlabel.Text = temp + ",- Kč";
            }
        }

        private void aktualizaceKreditu()
        {
            StreamWriter str = new StreamWriter("credit.bin");
            labelKredit.Text = "Kredit: " + credit + ",- Kč";
            aboutKreditHeaderLabel.Visible = true;
            aboutKreditlabel.Visible = true;
            aboutKreditlabel.Text = credit + ",- Kč";
            labelKredit.Visible = true;
            str.WriteLine(credit);
            str.Close();
        }

        private void kontrolaUctu()
        {
            if (File.Exists("account.bin"))
            {
                nastav = Serializace.deserializace();
                priradUcet();
            }
            else
            {
                LoginForm a = new LoginForm();
                a.nastavInstanci(this);
                a.Show();
                a.TopMost = true;
                this.Enabled = false;
            }
        }

        public void nastavUcet(NastavLogin login)
        {
            nastav = login;
            priradUcet();
        }

        private void buttonOdeslatSMS_Click(object sender, EventArgs e)
        {
            try
            {
                sms.Recipient = textBoxPrijemce.Text;
                sms.Text = odstranDiakritiku(textBoxZprava.Text);
                sms.RequireDeliveryReport = true;


                SmsGateReciever.SendSmsResult a = SmsGateReciever.SendSms(sms, log);
                credit = a.Credit.ToString();
                credit = (float.Parse(credit) * 1.20).ToString("#.##");
                aktualizaceKreditu();
                hlasky(a);
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Hlášky po odeslání zprávy
        /// </summary>
        /// <param name="result">Výsledek odeslání zprávy</param>
        private void hlasky(SmsGateReciever.SendSmsResult result)
        {
            #region ŠpatnýLogin
            if (result.Status == SmsGateReciever.SendSmsResultStatus.InvalidLogin)
            {
                DialogResult dialogResult;

                if (File.Exists("account.bin"))
                {
                    dialogResult = MessageBox.Show("Špatně zadané uživatelské údaje, prosím překontrolujte své údaje a zkuste to znovu.\n\nPřejete si smazat uložené údaje a zkusit přihlášení znovu?", "Špatné údaje", MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                    if (dialogResult == DialogResult.Yes)
                    {
                        try
                        {
                            File.Delete("account.bin");
                            kontrolaUctu();
                        }
                        catch (Exception e2)
                        {
                            MessageBox.Show(e2.Message, "Chyba při mazání", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    dialogResult = MessageBox.Show("Špatně zadané uživatelské údaje, prosím překontrolujte své údaje a zkuste to znovu.\n\n\nPřejete si zkusit přihlášení znovu?", "Špatné údaje", MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                    if (dialogResult == DialogResult.Yes)
                        kontrolaUctu();
                }
            }
            #endregion

            #region ZprávaOdeslána
            if (result.Status == SmsGateReciever.SendSmsResultStatus.OK)
            {
                MessageBox.Show("Zpráva byla úspěšně odeslána!\n\nCena SMS: " + result.Price + " kč\nZbývající kredit: " + (double)result.Credit * 1.2 + " kč\nPočet částí: " + result.Parts, "Zpráva odeslána", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            #endregion

            #region Chybný text
            if (result.Status == SmsGateReciever.SendSmsResultStatus.InvalidText)
            {
                MessageBox.Show("Chybný text zprávy! Prosím upravte text a zkuste odeslání znovu!", "Chybný text", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion

            #region Málo kreditu
            if (result.Status == SmsGateReciever.SendSmsResultStatus.LowCredit)
            {
               DialogResult res = MessageBox.Show("Na Vašem účtě není dostatek kreditu pro odeslání této zprávy!\n\nPřejete si otevřít stránky pro dobití kreditu?", "Nedostatek kreditu", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
               if (res == System.Windows.Forms.DialogResult.Yes)
               {
                   try
                   {
                       Process.Start("http://sms.sluzba.cz");
                   }
                   catch (Exception)
                   {
                       MessageBox.Show("Chyba při otevírání stránky", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   }
               }
            }
            #endregion
        }

        private void buttonAddContact_Click(object sender, EventArgs e)
        {
            if (textBoxPridatCislo.Text.Length != 0 && textBoxPridatJmeno.Text.Length != 0)
            {
                string res = kontakt.addContact(textBoxPridatJmeno.Text, textBoxPridatCislo.Text);

                if (res != "OK")
                    MessageBox.Show(res, "Chyba při vkládání", MessageBoxButtons.OK, MessageBoxIcon.Error);

                aktualizaceTabulky();
                textBoxPridatCislo.Clear();
                textBoxPridatJmeno.Clear();
                SerializaceContact.serializuj(kontakt);
            }
        }

        private void aktualizaceTabulky()
        {
            listView1.Items.Clear();
            ListViewItem listViewItem;

            foreach (var item in kontakt.getContacts)
            {
                listViewItem = new ListViewItem();
                listViewItem.Text = item.Value;
                listViewItem.SubItems.Add(item.Key);
                listView1.Items.Add(listViewItem);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListView lv = (ListView)sender;
                string cislo = lv.FocusedItem.SubItems[1].Text;
                textBoxPrijemce.Text = cislo;
                kontrolaVyplnenychUdaju();
                key = cislo;
                buttonOdebratKontakt.Enabled = true;
            }
            catch (Exception)
            {
                //todo
            }
        }

        private void buttonOdebratKontakt_Click(object sender, EventArgs e)
        {
            if (key != null)
            {
                bool res = kontakt.removeContact(key);
                SerializaceContact.serializuj(kontakt);
                aktualizaceTabulky();
                key = null;
                buttonOdebratKontakt.Enabled = false;
                textBoxPrijemce.Clear();
            }
        }

        private void textBoxZprava_TextChanged(object sender, EventArgs e)
        {
            kontrolaVyplnenychUdaju();
            labelPocetZnaku.Text = "Počet znaků: " + textBoxZprava.Text.Length;

            int pocetbezMezer = 0;
            for (int i = 0; i < textBoxZprava.Text.Length; i++)
            {
                if (textBoxZprava.Text[i] != ' ')
                    pocetbezMezer++;
            }

            labelPocetZnakuBezMezer.Text = "Bez mezer: " + pocetbezMezer.ToString();

            if (textBoxZprava.Text.Length == 161)
                toolTip1.Show("Zpráva bude odeslána na 2 části", textBoxZprava, 150, 95, 10000);
        }

        private void kontrolaVyplnenychUdaju()
        {
            if (textBoxZprava.Text.Length != 0 && textBoxPrijemce.Text.Length != 0)
            {
                buttonOdeslatSMS.Enabled = true;
            }
            else
            {
                buttonOdeslatSMS.Enabled = false;
            }
        }

        private void buttonOdeslaneSMSUpdate_Click(object sender, EventArgs e)
        {
            tabulkaOdeslaneSMS.Items.Clear();
            ListViewItem listViewItem;

            listSMS = SmsGateSender.GetOutgoingSmsList(30, log);
            listSMS.Reverse();

            foreach (var item in listSMS)
            {
                listViewItem = new ListViewItem();
                listViewItem.Text = item.Recipient;
                listViewItem.SubItems.Add(item.Text);
                listViewItem.SubItems.Add(item.SendAt.Value.ToString());
                tabulkaOdeslaneSMS.Items.Add(listViewItem);
            }
        }

        private void tabulkaOdeslaneSMS_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListView lv = (ListView)sender;
                string mess = "Zpráva:\n" + lv.FocusedItem.SubItems[1].Text + "\n\nPříjemce:\n" + lv.FocusedItem.SubItems[0].Text;
                mess += "\n\nOdesláno:\n" + lv.FocusedItem.SubItems[2].Text;

                MessageBox.Show(mess, "Detail zprávy", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                // todo
            }
        }

        /// <summary>
        /// Odstranění diakritiky pro bezproblémové odeslání zprávy
        /// </summary>
        /// <param name="text">Text zprávy</param>
        /// <returns>Zpráva bez diakritiky</returns>
        private string odstranDiakritiku(string text)
        {
            StringBuilder temp = new StringBuilder(text);
            temp.Replace('ě', 'e');
            temp.Replace('š', 's');
            temp.Replace('č', 'c');
            temp.Replace('ř', 'r');
            temp.Replace('ž', 'z');
            temp.Replace('ý', 'y');
            temp.Replace('á', 'a');
            temp.Replace('í', 'i');
            temp.Replace('é', 'e');
            temp.Replace('ď', 'd');
            temp.Replace('Ě', 'E');
            temp.Replace('Š', 'S');
            temp.Replace('Č', 'C');
            temp.Replace('Ř', 'R');
            temp.Replace('Ž', 'Z');
            temp.Replace('Ý', 'Y');
            temp.Replace('Á', 'A');
            temp.Replace('Í', 'I');
            temp.Replace('É', 'E');
            temp.Replace('Ď', 'D');

            string vrat = null;

            if (odeslatBezMezer)
                vrat = odstranitMezery(temp.ToString());
            else
                vrat = temp.ToString();

            return vrat;
        }

        private void odeslatBezMezerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            odeslatBezMezer = odeslatBezMezerToolStripMenuItem.Checked;

            if (odeslatBezMezer)
            {
                odeslatBezMezerToolStripMenuItem.Checked = false;
                odeslatBezMezer = false;
            }
            else
            {
                odeslatBezMezerToolStripMenuItem.Checked = true;
                odeslatBezMezer = true;
            }
        }

        /// <summary>
        /// Odstranění mezer a za mezeru vloží veliké písmeno pro lepčí čitelnost
        /// </summary>
        /// <param name="text">Text pro odstranění mezer</param>
        /// <returns>String bez meter</returns>
        private string odstranitMezery(string text)
        {
            string tmp = null;
            bool isFirst = true;

            for (int i = 0; i < text.Length; i++)
            {

                if (text[i] != ' ')
                {
                    if (isFirst == true)
                    {
                        tmp += text[i].ToString().ToUpper();
                    }
                    else
                    {
                        tmp += text[i].ToString().ToLower();
                    }

                    isFirst = false;
                }

                if (text[i] == ' ')
                    isFirst = true;
            }
            return tmp;
        }

        /// <summary>
        /// Aktualizační zpráva s informací že je k dispozici nová verze programu
        /// </summary>
        private void aktualizaceVerze()
        {
            WebClient wc = new WebClient();
            string verze = Application.ProductVersion.ToString();

            try
            {
                verze = wc.DownloadString("http://vym0008.vymak.cz/sms/src/version.txt");
                verze = verze.TrimEnd('\r', '\n');
            }
            catch (Exception)
            {
                MessageBox.Show("Chyba při stahování informací o nové verzi.", "Chyba stahování informací", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (Application.ProductVersion.ToString() != verze)
            {
                DialogResult akt = MessageBox.Show("Je k dispozici nová verza aplikace (" + verze + ").\nPřejete si novou verzi stáhnout?", "Aktualizace", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (DialogResult.Yes == akt)
                {
                    if (stahni())
                    {
                        Process.Start("SMSPosilac.exe");
                        Environment.Exit(4545);
                    }
                }
            }
            else
            {
                MessageBox.Show("Používáte aktuální verzi aplikace.", "Žádná aktualizace k dispozici", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Stažení nové verze programu a následné znovu spuštění nové verze
        /// </summary>
        /// <returns>Je li v pořádku staženo vrátí TRUE</returns>
        private bool stahni()
        {
            string nazev = "SMSPosilacNew.exe";

            try
            {
                WebClient wc = new WebClient();
                wc.DownloadFile("http://vym0008.vymak.cz/sms/src/SMSPosilac.exe", nazev);

                if (File.Exists(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName))
                    File.Replace(nazev, System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, "SMSPosilacBackup.exe");

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Chyba při stahování nové verze:\n" + e.Message, "Chyba aktualizace", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
