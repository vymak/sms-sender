﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.Classes
{
    [Serializable]
    public class Contact
    {
        private Dictionary<string, string> kontakty;

        public Contact()
        {
            kontakty = new Dictionary<string, string>();
        }

        public string addContact(string jmeno, string telefon)
        {
            try
            {
                kontakty.Add(telefon, jmeno);
            }
            catch (ArgumentException e)
            {
                return e.Message + "\nProsím změňte telefonní číslo a zkuste to znovu!";
            }
            return "OK";
        }

        public bool removeContact(string key)
        {
            try
            {
                kontakty.Remove(key);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        public Dictionary<string, string> getContacts
        {
            get { return this.kontakty; }
        }
    }
}
