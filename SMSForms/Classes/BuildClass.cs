﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.Classes
{
    public class BuildClass
    {
        /// <summary>
        /// Variable with build date
        /// </summary>
        private static DateTime buildDate = new DateTime(2012,11,19,15,16,56);

        /// <summary>
        /// Property return build date
        /// </summary>
        public static DateTime BuildDate
        {
            get { return buildDate; }
        }
    }
}
