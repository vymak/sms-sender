﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace WindowsFormsApplication1.Classes
{
    public static class SerializaceContact
    {/*
        public static void serializuj(Contact kont)
        {
            Stream lStream = new FileStream("contact.bin", FileMode.Create);
            IFormatter lFormatter = new BinaryFormatter();

            try
            {
                lFormatter.Serialize(lStream, kont);
            }
            finally
            {
                lStream.Close();
            }
        }

        public static Contact deserializace()
        {
            Contact kont = new Contact();
            IFormatter lFormatter = new BinaryFormatter();
            Stream lStream = new FileStream("contact.bin", FileMode.Open);

            try
            {
                kont = (Contact)lFormatter.Deserialize(lStream);
                return kont;
            }
            finally
            {
                lStream.Close();
            }
        }
      * */

        private static byte[] key = { 8, 5, 1, 9, 2, 4, 9, 2 }; // Where to store these keys is the tricky part, you may need to obfuscate them or get the user to input a password each time
        private static byte[] iv = { 1, 9, 4, 2, 13, 48, 1, 8 };
        private static string path = "contact.bin";

        public static void serializuj(Contact kont)
        {

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            try
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    var cryptoStream = new CryptoStream(fs, des.CreateEncryptor(key, iv), CryptoStreamMode.Write);
                    BinaryFormatter formatter = new BinaryFormatter();


                    formatter.Serialize(cryptoStream, kont);
                    cryptoStream.FlushFinalBlock();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static Contact deserializace()
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            try
            {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    var cryptoStream = new CryptoStream(fs, des.CreateDecryptor(key, iv), CryptoStreamMode.Read);
                    BinaryFormatter formatter = new BinaryFormatter();

                    // This is where you deserialize the class
                    Contact deserialized = (Contact)formatter.Deserialize(cryptoStream);

                    return deserialized;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
