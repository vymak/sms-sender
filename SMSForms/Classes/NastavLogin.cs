﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.Classes
{
    [Serializable]
    public class NastavLogin
    {
        private string username, password;

        /// <summary>
        /// Nastavení uživatelských údajů
        /// </summary>
        /// <param name="username">Uživatelské jméno</param>
        /// <param name="password">Heslo</param>
        public void nastavUzivatelskeUdaje(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        /// <summary>
        /// Vrací uživatelské jméno pro přihlášení
        /// </summary>
        public string getUsername
        {
            get { return this.username; }
        }

        /// <summary>
        /// Vrací heslo pro přihlášení
        /// </summary>
        public string getPassword
        {
            get { return this.password; }
        }
    }
}
