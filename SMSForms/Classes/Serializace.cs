﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace WindowsFormsApplication1.Classes
{
    public static class Serializace
    {
        private static byte[] key = { 5, 8, 3, 7, 1, 6, 9, 2 }; // Where to store these keys is the tricky part, you may need to obfuscate them or get the user to input a password each time
        private static byte[] iv = { 8, 7, 3, 4, 2, 6, 1, 8 };
        private static string path = "account.bin";

        public static void serializuj(NastavLogin log){

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            try
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    var cryptoStream = new CryptoStream(fs, des.CreateEncryptor(key, iv), CryptoStreamMode.Write);
                    BinaryFormatter formatter = new BinaryFormatter();


                    formatter.Serialize(cryptoStream, log);
                    cryptoStream.FlushFinalBlock();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static NastavLogin deserializace()
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            try
            {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    var cryptoStream = new CryptoStream(fs, des.CreateDecryptor(key, iv), CryptoStreamMode.Read);
                    BinaryFormatter formatter = new BinaryFormatter();

                    // This is where you deserialize the class
                    NastavLogin deserialized = (NastavLogin)formatter.Deserialize(cryptoStream);

                    return deserialized;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
