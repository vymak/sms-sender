﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Posílač SMS pro sms.sluzba.cz ")]
[assembly: AssemblyDescription("Posílání a veškerá správa SMS pro službu sms.sluzba.cz")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vymak")]
[assembly: AssemblyProduct("SMS")]
[assembly: AssemblyCopyright("Copyright © Vymak  2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8d1deafa-14a2-43f2-8753-ed40aa76d360")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.4.1.8")]
[assembly: AssemblyFileVersion("1.4.1.8")]
